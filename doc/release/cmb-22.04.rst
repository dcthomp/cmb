.. _release-notes-22.04:

=======================
CMB 22.04 Release Notes
=======================

See also :ref:`release-notes-21.12` for previous changes.

Simplifying Branding CMB-based Applications
===========================================
CMB Refactored to be a template SMTK application
------------------------------------------------

Application details such as icons and naming are now exposed via
CMake variables. The pqAboutDialogReaction and default LayoutSpec
may be overridden using application specific implementations.

This will make it easier for developers to created custom CMB-based applications with minimal code duplication.


UI Improvements
===============

Animation Features
------------------

The ParaView animation features are now including in the postprocessing
plugin. When postprocessing is turned on, the Animation View is available
in the "View" menu, and a "Save Animation..." item is available in the
"File" menu.

Catalyst menu during post-process
---------------------------------

When post-process mode is active, show the Catalyst menu, and two more
entries in the File menu, "Save Catalyst State" and "Save Extracts"

Fixed Postprocessing Mode
-------------------------
All of ParaView's Sources and Filters have been restored when in **postprocessing** mode.


SMTK Related Changes
====================
For additional information on changes to SMTK, please see `SMTK-22.04 <https://smtk.readthedocs.io/en/latest/release/smtk-22.04.html>`_.
