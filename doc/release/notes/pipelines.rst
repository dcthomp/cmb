Pipeline objects
----------------

Upcoming changes to SMTK will provide CMB a way to force all resources
to have an associated pipeline object. When SMTK is new enough, use
this mechanism so that File→Save, File→Save as…, File→New, File→Close resource,
and other behaviors/reactions that require a pipeline object will work.

These behaviors were broken by an older change that avoided creating
pipeline objects if no renderable data existed. Until the behaviors/reactions
can be updated, we provide this fix.
