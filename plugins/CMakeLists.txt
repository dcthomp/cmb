paraview_plugin_scan(
  PLUGIN_FILES postprocessing-mode/paraview.plugin
  PROVIDES_PLUGINS paraview_plugins
  ENABLE_BY_DEFAULT ON
  HIDE_PLUGINS_FROM_CACHE ON)

get_property(_cmb_application_NAME GLOBAL PROPERTY _cmb_application_NAME)
get_property(_cmb_application_VERSION GLOBAL PROPERTY _cmb_application_VERSION)

paraview_plugin_build(
  PLUGINS ${paraview_plugins}
  PLUGINS_FILE_NAME "${_cmb_application_NAME}-postprocess.xml"
  INSTALL_EXPORT CMBParaViewPlugins
  RUNTIME_DESTINATION ${CMAKE_INSTALL_BINDIR}
  CMAKE_DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${_cmb_application_NAME}
  LIBRARY_SUBDIRECTORY "${_cmb_application_NAME}-${_cmb_application_VERSION}"
  TARGET cmb_paraview_plugins)
