cmake_minimum_required(VERSION 3.12)

# Tarballs are uploaded by CI to the "ci/smtk" folder, In order to use a new superbuild, please move
# the item from the date stamped directory into the `keep` directory. This makes them available for
# download without authentication and as an indicator that the file was used at some point in CI
# itself.

set(data_host "https://data.kitware.com")

# Determine the tarball to download. ci-cmb-ci-developer-{date}-{git-sha}.tar.gz
# 20250105 - Update to use Xcode 16.1
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2022")
  set(file_id "148oE3iQDmhD0XQr4ucZOA4FLddun7V8E")
  set(file_hash "86b53b4f21770affdc31db4f611e65764f3734a6f32eb3a89d70924de29f4a19c0f1e2a7c308ac70b6e6826ebe6f89533138e549f749f21574d17ba240bf8932")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos_arm64")
  set(file_id "1nRJDU9yR60V53uqDdm-mCgwLY7Z7blJP")
  set(file_hash "8fcf554f20d68f70ca4268b51cff14a9639e06b0ee038e6562f7a4985f657b31161610e9dce54635545a713b79af4c64db49e52aace6fc789bf016f180b55b2a")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos_x86_64")
  set(file_id "1_nONB7_bomK3zKt-ihidy1dghwIpDpBv")
  set(file_hash "84b3f967d9b2e4fc41c47220240c7f66110717150f949a7e5a8bb4b53f1bdd774299c0d72eb41db4a33b1f41598718e2981e99cf43078fbf12058e94e969a39f")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_id OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "https://drive.usercontent.google.com/download?export=download&id=${file_id}&export=download&authuser=0&confirm=t"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
