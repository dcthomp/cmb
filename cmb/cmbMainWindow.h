//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef cmbMainWindow_h
#define cmbMainWindow_h

#include <QMainWindow>

/// MainWindow for the default ParaView-based application.
class cmbMainWindow : public QMainWindow
{
  Q_OBJECT
  using Superclass = QMainWindow;

public:
  cmbMainWindow();
  ~cmbMainWindow() override;

  /**\brief Override the base popup menu so test playback can work.
    *
    * We override the parent method in order to set the object name
    * of the created menu; otherwise, tests will fail because the
    * name is not consistent across runs.
    */
  QMenu* createPopupMenu() override;

public Q_SLOTS:
  /**\brief A slot called shortly after construction.
    *
    * This function continues setup after ParaView-specific
    * setup has occurred. Some ParaView-specific setup only
    * occurs once the event loop is running.
    */
  void prepare();
  /// Enable post-processing-specific toolbars and menus.
  void togglePostProcessingMode(bool enablePostProcessing);

protected Q_SLOTS:
  void dragEnterEvent(QDragEnterEvent* evt) override;
  void dropEvent(QDropEvent* evt) override;
  void showEvent(QShowEvent* evt) override;
  void closeEvent(QCloseEvent* evt) override;

private Q_SLOTS:
  void postParaViewSetup();

protected:
  class pqInternals;
  pqInternals* Internals;

  static void testSetup();

  static bool registerFactoryOverrides;

private:
  Q_DISABLE_COPY(cmbMainWindow)
};

#endif
