//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef modelbuilder_mbTestEventPlayer_h
#define modelbuilder_mbTestEventPlayer_h

#include "pqWidgetEventPlayer.h"

/**\brief Subclass of pqWidgetEventPlayer that handles recorded file dialog events.
  *
  * \sa pqEventPlayer
  */
class cmbTestEventPlayer : public pqWidgetEventPlayer
{
  Q_OBJECT
public:
  cmbTestEventPlayer(QObject* p = nullptr);

  bool playEvent(QObject* Object, const QString& cmd, const QString& args, bool& Error) override;

  cmbTestEventPlayer(const cmbTestEventPlayer&) = delete;
  cmbTestEventPlayer& operator=(const cmbTestEventPlayer&) = delete;
};

#endif
