# We require cmb's version information to be included the CMake Project
# declaration in order to properly include cmb versions when using cmb as an
# SDK. We therefore do not have access to the variable ${PROJECT_SOURCE_DIR}
# at the time cmbVersion.cmake is read. We therefore use
# ${CMAKE_CURRENT_SOURCE_DIR}, since CMBVersion.cmake is called at the top
# level of cmb's build. We also guard against subsequent calls to
# CMBVersion.cmake elsewhere in the build setup where
# ${CMAKE_CURRENT_SOURCE_DIR} may no longer be set to the top level directory.

function (cmb_parse_version_file name)

  cmake_parse_arguments(_parse_version "EXCLUDE_PATCH_EXTRA" "FILE" "" ${ARGN})

  if (NOT _parse_version_FILE)
    set(_parse_version_FILE "${CMAKE_CURRENT_SOURCE_DIR}/version.txt")
  endif ()

  if (NOT DEFINED "${name}_version")
    set(_version_patch_extra "${CMAKE_BUILD_TYPE}")

    if (EXISTS "${_parse_version_FILE}")
      file(STRINGS "${_parse_version_FILE}" version_string )

      string(REGEX MATCH "([0-9]+)\\.([0-9]+)\\.([0-9]+)[-]*(.*)"
        version_matches "${version_string}")

      set(_version_major ${CMAKE_MATCH_1})
      set(_version_minor ${CMAKE_MATCH_2})
      set(_version_patch "${CMAKE_MATCH_3}")
      if (CMAKE_MATCH_4)
        set(_version_patch_extra "${CMAKE_MATCH_4}")
      endif()
    else()
      message(WARNING "No version.txt detected, defaulting version to 1.0.0")
      set(_version_major 1 PARENT_SCOPE)
      set(_version_minor 0 PARENT_SCOPE)
      set(_version_patch 0 PARENT_SCOPE)
    endif()
    if (_parse_version_EXCLUDE_PATCH_EXTRA)
      set(_version "${_version_major}.${_version_minor}.${_version_patch}")
    else ()
      set(_version "${_version_major}.${_version_minor}.${_version_patch}-${_version_patch_extra}")
    endif ()

    set("${name}_version" ${_version} PARENT_SCOPE)
    set("${name}_version_major" ${_version_major} PARENT_SCOPE)
    set("${name}_version_minor" ${_version_minor} PARENT_SCOPE)
    set("${name}_version_patch" ${_version_patch} PARENT_SCOPE)
    set("${name}_version_patch_extra" ${_version_patch_extra} PARENT_SCOPE)
  endif()

endfunction ()
